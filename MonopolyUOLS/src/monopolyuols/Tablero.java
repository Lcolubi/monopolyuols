/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;

/**
 *
 * @author lcolubi
 */
public class Tablero {
        //la var turnoActual es la id del Jugador (número de jugador)
	int turnoActual = 0;
        //Total de jugadores
	int numeroJugadores = 0;
        String[] nombresJugadores;
	Jugador[] jugadores;    
        Casilla[] casillas = new Casilla[40];
        
        //Dado
        int resultadoDado;

	public Tablero(int numeroJugadores, String[] nombresJugadores) {
		jugadores= new Jugador[numeroJugadores];
		this.numeroJugadores = numeroJugadores;
                this.nombresJugadores = nombresJugadores;
		for(int i = 0;i < jugadores.length;i++){
                    jugadores[i] = new Jugador(i, nombresJugadores[i]);
		}
         //Definimos Casillas Especiales
        /*o	Casillas Especiales (entre paréntesis el número de casilla).
•	(1) Salida (Cobra 200€, si aún existen propiedades libres)
•	(3) Caja de Comunidad
•	(5) Impuesto Sobre el Capital (El jugador pierde el 10% de su capital y se deposita en el fondo de la banca)
•	(8) Suerte
•	(11) Casilla Cárcel
•	(18) Caja de Comunidad
•	(21) Estacionamiento Gratuito
•	(23) Suerte
•	(31) Ve a la Cárcel
•	(34) Caja de Comunidad
•	(37) Suerte
•	(39) Impuesto de lujo */
                
                casillas[0]= new Casilla("Salida");
                casillas[2]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[4]= new CasillaImpuestoCapital("Impuesto sobre el capital");
                casillas[7]= new CasillaSuerte("Suerte");
                casillas[10]= new CasillaCarcel("Carcel");
                casillas[17]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[20]= new CasillaEstacionamientoGratuito("Estacionamiento gratuito");
                casillas[22]= new CasillaSuerte("Suerte");
                casillas[30]= new CasillaIrACarcel("Ve a la Cárcel");
                casillas[33]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[36]= new CasillaSuerte("Suerte");
                casillas[38]= new CasillaImpuestoLujo("Impuesto de Lujo");
                
         //Definimos casillas de estaciones y compañias electricas
                casillas[5]= new CasillaEstacion("Estación 1");
                casillas[15]=new CasillaEstacion("Estación 2");
                casillas[25]= new CasillaEstacion("Estación 3"); 
                casillas[35]=new CasillaEstacion("Estación 4");
                casillas[12]= new CasillaCompañia("Compañía de electricidad");
                casillas[28]= new CasillaCompañia("Compañía de Aguas");
         // A continuación habría que definir el resto de casillas (calles que serían propiedades)       
	}
	
	public Jugador getJugadorActual() {
		return jugadores[turnoActual];
	}
	
	public Jugador[] getJugadores() {
		return jugadores;
	}
	
	public void siguienteTurno() {
		if(++turnoActual >= jugadores.length){
			turnoActual = 0;
		}
	}

	public Jugador getJugador(int id) {
		return jugadores[id];
	}
        public int getResultadoDado() {
		Random rand = new Random();
		int resultadoDado = 1+rand.nextInt(6);
		return resultadoDado;
	}
        
        public int[] tirarDados () {
        int dado1 = this.getResultadoDado();
        int dado2 = this.getResultadoDado();
        int total = dado1+dado2;
        //System.out.println("\t\033[32mHas sacado un "+dado1+" y un "+dado2);
        //System.out.println("\t\033[32mAvanza "+total+" casillas"); 
        int [] resultados  = new int [3];
        resultados[0]=dado1;
        resultados[1]=dado2;
        resultados[2]=total;
        return resultados;
        }


}
