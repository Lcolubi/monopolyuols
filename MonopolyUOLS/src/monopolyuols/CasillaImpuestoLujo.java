/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class CasillaImpuestoLujo extends Casilla {
    	public CasillaImpuestoLujo(String nombre) {
		super(nombre);
	}
	
	public void accion(Jugador jugador) {
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de Lujo y se le restan 100 euros");
                jugador.restarDinero(100);
                System.out.println(jugador.getName()+" se queda con "+jugador.getDinero()+" euros.");
                
	}
}
