/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;

/**
 *
 * @author lcolubi
 */
public class CasillaCajaComunidad extends Casilla {
	public CasillaCajaComunidad(String nombre) {
		super(nombre);
	}
	
	public void accion(Jugador jugador, Jugador [] jugadoresTablero) {
                Random rand = new Random();
		int resultadoSuerte = 1+rand.nextInt(16);
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de Suerte y recibe la siguiente tarjeta:");    
                
                switch (resultadoSuerte) {
                    case 1:
                        System.out.println("\\t\\033[32mMueve hasta la casilla de salida");
                        //Metodo funcional
                        
                        break;
                    case 2:
                        System.out.println("\\t\\033[32mError del banco ganas 200€.");
                        //Metodo funcional
                        jugador.añadirDinero(200);
                        break;
                        
                    case 3:
                        System.out.println("\\t\\033[32mPagas al dentista 50€.");
                        //Metodo funcional
                        jugador.restarDinero(50);
                        break;
                                
                    case 4:
                        System.out.println("\\t\\033[32mGanas 50€ de dividendos");
                        //Metodo funcional
                        jugador.añadirDinero(50);
                        break;  
                        
                    case 5:
                        System.out.println("\\t\\033[32mQueda libre de la cárcel, esta carta se puede usar cuando se crea oportuno. No se puede vender.");
                        //Metodo funcional
                        jugador.añadirCartaLibreCarcel();
                        break; 
                        
                    case 6:
                        System.out.println("\\t\\033[32mVe a la cárcel.");
                        //Metodo funcional
                        jugador.encarcelar();
                        break;  
                        
                    case 7:
                        System.out.println("\\t\\033[32mGran estreno en la ópera, cada jugador te paga 50€.");
                        //Metodo funcional
                        for (int i=0; i<jugadoresTablero.length;i++) {
                            if (jugadoresTablero[i].getID() != jugador.getID()){
                                jugadoresTablero[i].restarDinero(50);
                                jugador.añadirDinero(50);
                            }
                        }
                        break;           
                    case 8:
                        System.out.println("\\t\\033[32mDividendos. Ganas 100 Euros");
                        //Metodo funcional
                        jugador.añadirDinero(100);
                        //mover a la carcel:
                        break; 
                        
                    case 9:
                        System.out.println("\\t\\033[32mDevolución de impuestos. Ganas 20 Euros");
                        //Metodo funcional
                        jugador.añadirDinero(20);
                        break;  
                        
                    case 10:
                        System.out.println("\\t\\033[32m¡Feliz cumpleaños! Ganas 10 Euros de cada jugador");
                        //Metodo funcional
                        for (int i=0; i<jugadoresTablero.length;i++) {
                            if (jugadoresTablero[i].getID() != jugador.getID()){
                                jugadoresTablero[i].restarDinero(10);
                                jugador.añadirDinero(10);
                            }
                        }
                        break;    
                        
                    case 11:
                        System.out.println("\\t\\033[32mPrimer premio en la loteria. Ganas 100 Euros");
                        //Metodo funcional
                        jugador.añadirDinero(100);
                        break;  
                        
                    case 12:
                        System.out.println("\\t\\033[32mMulta de velocidad. Pagas 100 Euros");
                        //Metodo funcional
                        jugador.restarDinero(100);
                        break;
                    
                    case 13:
                        System.out.println("\\t\\033[32mMatricula escolar, pagas 150 Euros");
                        jugador.restarDinero(150); 
                        break;
                        
                    case 14:
                        System.out.println("\\t\\033[32mTasa de consultor, ganas 25 Euros");
                        jugador.añadirDinero(25);   
                        break;
                        
                    case 15:
                        System.out.println("\\t\\033[32mReparaciones. Pagas 40 euros por cada casa que tengas y 150 Euros por cada Hotel");
                        //METODO A IMPLEMENTAR
                        
                        break;
                        
                    case 16:
                        System.out.println("\\t\\033[32mGanas el segundo premio en un concurso de disfraces ganas 10€. ");
                        jugador.añadirDinero(10);
                }
	}
}
