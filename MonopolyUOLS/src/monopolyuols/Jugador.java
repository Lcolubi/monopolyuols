/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class Jugador {
    
        int totalTurnosJugados = 0;
	int posicion = 0;
	int id;
	String nombre;
        //Dinero inicial --> 1500 euros
	int dinero = 1500;
        // Saber si está encarcelado
        boolean encarcelado = false;
        //Carta Suerte Liberar de Carcel
        int cartaLibreCarcel = 0;
        //Numero de Estaciones y compañias que posee
        int numeroEstaciones = 0;
        int numeroComp = 0;
        
	
	public Jugador(int id, String name) {
		this.id = id;
		this.nombre = name;
	}
	public boolean getEncarcelado (){
            return encarcelado;
        }
        public void encarcelar(){
            this.encarcelado=true;
        }
        public void desencarcelar() {
            this.encarcelado=false;
        }
        public int getCartaLibreCarcel () {
            return this.cartaLibreCarcel;
        }
        public void gastarCartaLibreCarcel () {
            this.cartaLibreCarcel --;
        }
        public void añadirCartaLibreCarcel(){
            this.cartaLibreCarcel ++;
        }
        
	public int getTurnosJugados() {
		return totalTurnosJugados;
	}
	
	public int getCurrentPosition() {
		return posicion;
	}
	
	public void setPosition(int posicion) {
		this.posicion = posicion;
	}
	
	public void nextTurn() {
		totalTurnosJugados++;
	}
	
	public String getName() {
		return nombre;
	}
	
	public int getDinero() {
		return this.dinero;
	}
        
        public void añadirDinero(int cantidad) {
		this.dinero += cantidad;
	}
	
	public void restarDinero(int cantidad) {
		this.dinero -= cantidad;
	}
	public int getID() {
		return id;
	}
	public int getNumEstaciones() {
		return this.numeroEstaciones;
	}
	public int getNumComp() {
		return this.numeroComp;
	}
        public void addEstacion () {
            this.numeroEstaciones++;
        }
        public void addComp() {
            this.numeroComp++;
        }

}
