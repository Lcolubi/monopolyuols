/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;

/**
 *
 * @author lcolubi
 */
public class CasillaSuerte extends Casilla {
	public CasillaSuerte(String nombre) {
		super(nombre);
	}
	
	public void accion(Jugador jugador, Jugador [] jugadoresTablero) {
                Random rand = new Random();
		int resultadoSuerte = 1+rand.nextInt(15);
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de Suerte y recibe la siguiente tarjeta:");    
                
                switch (resultadoSuerte) {
                    case 1:
                        System.out.println("\\t\\033[32mMueve hasta la casilla de salida");
                        //Metodo funcional
                        
                        break;
                    case 2:
                        System.out.println("\\t\\033[32mMueve hasta la última propiedad del tablero.");
                        //Metodo funcional
                        
                        break;
                        
                    case 3:
                        System.out.println("\\t\\033[32mMueve hasta la casilla de servicio público más próxima, si tiene propietario paga como si hubieras sacado un 10 en la tirada. Si está libre puedes adquirirla.");
                        //Metodo funcional
                        
                        break;
                                
                    case 4:
                        System.out.println("\\t\\033[32mMueve hasta la estación más próxima, si tiene propietario págale el alquiler. Si está libre puedes adquirirla..");
                        //Metodo funcional
                        
                        break;                            
                    case 5:
                        System.out.println("\\t\\033[32mEl banco paga un dividendo de 50€.");
                        //Metodo funcional
                        jugador.añadirDinero(50);
                        
                        break;                
                    case 6:
                        System.out.println("\\t\\033[32mQueda libre de la cárcel, esta carta se puede usar cuando se crea oportuno. No se puede vender.");
                        //Metodo funcional
                        jugador.añadirCartaLibreCarcel();
                        
                        break;                
                    case 7:
                        System.out.println("\\t\\033[32mMueve hasta la estación más próxima, si tiene propietario págale el alquiler. Si está libre puedes adquirirla..");
                        //Metodo funcional
                        jugador.cartaLibreCarcel++;
                        break;             
                    case 8:
                        System.out.println("\\t\\033[32mRetrocede 3 casillas.");
                        //Metodo funcional
                        
                        break;           
                    case 9:
                        System.out.println("\\t\\033[32mVe a la cárcel, sin cobrar si se pasa por la casilla de salida.");
                        //Metodo funcional
                        jugador.encarcelar();
                        //mover a la carcel:
                        break;             
                    case 10:
                        System.out.println("\\t\\033[32mHaz reparaciones en tus propiedades, 25€ por casa y 100€ por hotel.");
                        //Metodo funcional
                        
                        break;             
                    case 11:
                        System.out.println("\\t\\033[32m+Paga impuestos 15€.");
                        //Metodo funcional
                        jugador.restarDinero(15);
                        break;             
                    case 12:
                        System.out.println("\\t\\033[32mVe a la primera estación del tablero.");
                        //Metodo funcional
                        
                        
                        break;             
                    case 13:
                        System.out.println("\\t\\033[32mCada jugador te paga 50€.");
                        //Metodo funcional
                        for (int i=0; i<jugadoresTablero.length;i++) {
                            if (jugadoresTablero[i].getID() != jugador.getID()){
                                jugadoresTablero[i].restarDinero(50);
                                jugador.añadirDinero(50);
                            }
                        }
                        break;
                    
                    case 14:
                        System.out.println("\\t\\033[32mDividendos, ganas 150€.");
                        jugador.añadirDinero(150); 
                        
                    case 15:
                        System.out.println("\\t\\033[32mGanas una competición de crucigramas (+100Euros)");
                        jugador.añadirDinero(100);
                }
	}
}
