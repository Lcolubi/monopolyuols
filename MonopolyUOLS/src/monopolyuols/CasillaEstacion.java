/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class CasillaEstacion extends Casilla {
        //ID DEL DUEÑO
        Jugador dueño = null;  
    
	public CasillaEstacion(String nombre) {
		super(nombre);
	}
        public void setDueño (Jugador dueño) {
            this.dueño = dueño;
        }
	
	public void accion(Jugador jugador) {
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de "+ super.getNombre());
                if (this.dueño == null) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("\\t\\033[32m¿Deseas comprar la Estación por 200 Euros? (S/N)");
                    char opcionCompra = scanner.nextLine().charAt(0);
                    if (opcionCompra == 's' || opcionCompra == 'S') {
                        jugador.restarDinero(200);
                        int idJugador = jugador.getID();
                        this.setDueño(jugador);
                        jugador.addEstacion();
                        System.out.println("\\t\\033[32mEl jugador "+jugador.getName()+" ha comprado la Casilla de "+super.getNombre()+" por 200 Euros.");
                    }
                    else {
                        System.out.println("\\t\\033[32mEl jugador "+jugador.getName()+" decide no comprar la Estación");
                    }
                }
                else {
                    System.out.println("\\t\\033[32mEsta casilla pertenece al Jugador "+this.dueño.getName()+"que tiene el siguiente número de estaciones: "+this.dueño.getNumEstaciones());
                    System.out.println("\\t\\033[32mDebes de pagarle un total de "+25*this.dueño.getNumEstaciones()+" Euros.");
                    jugador.restarDinero(25*this.dueño.getNumEstaciones());
                    this.dueño.añadirDinero(25*this.dueño.getNumEstaciones());
                }
	}
}
