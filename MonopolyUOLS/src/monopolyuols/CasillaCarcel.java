/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class CasillaCarcel extends Casilla {
	public CasillaCarcel(String nombre) {
		super(nombre);
	}
	
	public void accion(Jugador jugador) {
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de Carcel y ha realizado una visita gratuita");    
	}
}
