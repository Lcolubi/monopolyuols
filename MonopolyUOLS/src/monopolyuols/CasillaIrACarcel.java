/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class CasillaIrACarcel extends Casilla {
	public CasillaIrACarcel(String nombre) {
		super(nombre);
	}
	
	public void accion(Jugador jugador) {
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de IR A LA CÁRCEL y se le condena a permanecer 3 turnos en la casilla de Cárcel");    
                System.out.println("\\t\\033[32mPara salir de la cárcel deberá de pagar 50Euros o sacar Dobles en la tirada de alguno de los turnos");
                System.out.println("\\t\\033[32mSi el jugador no saca dobles en ninguno de los 3 turnos, deberá de pagar obligado los 50euros para salir.");
                // A continuación se llamarían a los metodos para mover al jugador a la casilla de carcel y aplicar las penalizaciones
                jugador.encarcelar();
                
                if (jugador.getCartaLibreCarcel()>0){
                System.out.println("\\t\\033[32m ¿Deseas utilizar tu carta de la Suerte para quedar libre de la Cárcel? (S/N)");
                Scanner scanner = new Scanner(System.in);
                String respuesta = scanner.nextLine().toLowerCase();
                if (respuesta == "s" || respuesta == "si") {
                    jugador.gastarCartaLibreCarcel();
                    jugador.desencarcelar();
                    System.out.println("Has sido liberado de la carcel gracias a tu carta de la suerte, en el siguiente turno estarás libre.");
                }
                }
	}
}
