/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class CasillaCompañia extends Casilla {
        //ID DEL DUEÑO
        Jugador dueño = null;  
    
	public CasillaCompañia(String nombre) {
		super(nombre);
	}
        public void setDueño (Jugador dueño) {
            this.dueño = dueño;
        }
	
	public void accion(Jugador jugador, int tiradaTotalDados) {
                System.out.println("\\t\\033[32mEl Jugador "+jugador.getName()+" ha caido en la casilla de "+ super.getNombre());
                if (this.dueño == null) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("\\t\\033[32m¿Deseas comprar la Compañía por 150 Euros? (S/N)");
                    char opcionCompra = scanner.nextLine().charAt(0);
                    if (opcionCompra == 's' || opcionCompra == 'S') {
                        jugador.restarDinero(150);
                        int idJugador = jugador.getID();
                        this.setDueño(jugador);
                        jugador.addComp();
                        System.out.println("\\t\\033[32mEl jugador "+jugador.getName()+" ha comprado la Casilla de "+super.getNombre()+" por 150 Euros.");
                    }
                    else {
                        System.out.println("\\t\\033[32mEl jugador "+jugador.getName()+" decide no comprar la Compañía");
                    }
                }
                else {
                    System.out.println("\\t\\033[32mEsta casilla pertenece al Jugador "+this.dueño.getName()+"que tiene el siguiente número de compañías: "+this.dueño.getNumComp());
                    
                    
                    if (this.dueño.getNumComp() == 1){
                        System.out.println("\\t\\033[32mDebes de pagarle un total de "+tiradaTotalDados*4+" Euros.");
                        jugador.restarDinero(tiradaTotalDados*4);
                        this.dueño.añadirDinero(tiradaTotalDados*4);
                    } else if (this.dueño.getNumComp() == 2) {
                        System.out.println("\\t\\033[32mDebes de pagarle un total de "+tiradaTotalDados*10+" Euros.");
                        jugador.restarDinero(tiradaTotalDados*10);
                        this.dueño.añadirDinero(tiradaTotalDados*10);
                    }

                }
	}
}
