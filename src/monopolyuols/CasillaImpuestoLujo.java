/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class CasillaImpuestoLujo extends Casilla {

    /**
     *
     * @param nombre
     */
    public CasillaImpuestoLujo(String nombre) {
		super(nombre);
	}

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            return true;
        }

    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de Lujo y se le restan 100 euros");
                jugador.restarDinero(100);
                System.out.println(jugador.getName()+" se queda con "+jugador.getDinero()+" euros.");
                
	}
}
