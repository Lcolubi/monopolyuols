/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author hp
 */
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteXMLFile {

	public static void writeXML (Tablero tablero) {

	  try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("partida");
		doc.appendChild(rootElement);
                
                
                Element casillaActual = doc.createElement("jugadores");
                casillaActual.appendChild(doc.createTextNode(""+tablero.getCasillaActual()));
		rootElement.appendChild(casillaActual);
                
                Element turnoActual = doc.createElement("turnoActual");
                turnoActual.appendChild(doc.createTextNode(""+tablero.getTurnoActual()));
		rootElement.appendChild(turnoActual);
                
                Element numeroJugadores = doc.createElement("numeroJugadores");
                numeroJugadores.appendChild(doc.createTextNode(""+tablero.getNumeroJugadores()));
		rootElement.appendChild(numeroJugadores);
                
                Element resultadoDado = doc.createElement("resultadoDado");
                resultadoDado.appendChild(doc.createTextNode(""+tablero.getResultadoDado()));
		rootElement.appendChild(resultadoDado);
                
		Element jugadores = doc.createElement("jugadores");
		rootElement.appendChild(jugadores);

                //Obtenemos datos de los jugadores
                for(int i=0; i< tablero.getNumeroJugadores(); i++) {
                    // jugador elements
                    Element jugador = doc.createElement("jugador");
                    jugadores.appendChild(jugador);

                    // set attribute to staff element
                    Attr attr = doc.createAttribute("id");
                    attr.setValue(Integer.toString(i));
                    jugador.setAttributeNode(attr);

                    // shorten way
                    // staff.setAttribute("id", "1");


                    Element nombre = doc.createElement("nombre");
                    nombre.appendChild(doc.createTextNode(tablero.getJugador(i).getName()));
                    jugador.appendChild(nombre);

                    Element totalTurnosJugados = doc.createElement("totalTurnosJugados");
                    totalTurnosJugados.appendChild(doc.createTextNode(""+tablero.getJugador(i).getTurnosJugados() ));
                    jugador.appendChild(totalTurnosJugados);

                    Element posicion = doc.createElement("posicion");
                    posicion.appendChild(doc.createTextNode(""+tablero.getJugador(i).getCurrentPosition() ));
                    jugador.appendChild(posicion);

                    Element dinero = doc.createElement("dinero");
                    dinero.appendChild(doc.createTextNode(""+tablero.getJugador(i).getDinero()));
                    jugador.appendChild(dinero);

                    if (tablero.getJugador(i).getEncarcelado()) {
                    Element encarcelado = doc.createElement("encarcelado");
                    encarcelado.appendChild(doc.createTextNode("isEncarcelado"));
                    jugador.appendChild(encarcelado);
                    }

                    Element cartaLibreCarcel = doc.createElement("cartaLibreCarcel");
                    cartaLibreCarcel.appendChild(doc.createTextNode(""+tablero.getJugador(i).getCartaLibreCarcel()));
                    jugador.appendChild(cartaLibreCarcel);

                    Element turnosEncarcelado = doc.createElement("turnosEncarcelado");
                    turnosEncarcelado.appendChild(doc.createTextNode(""+tablero.getJugador(i).getTurnosEncarcelado()));
                    jugador.appendChild(turnosEncarcelado);         

                    Element numeroEstaciones = doc.createElement("numeroEstaciones");
                    numeroEstaciones.appendChild(doc.createTextNode(""+tablero.getJugador(i).getNumEstaciones()));
                    jugador.appendChild(numeroEstaciones);

                    Element numeroComp = doc.createElement("numeroComp");
                    numeroComp.appendChild(doc.createTextNode(""+tablero.getJugador(i).getNumComp()));
                    jugador.appendChild(numeroComp);

                    Element ultimaTirada = doc.createElement("ultimaTirada");
                    ultimaTirada.appendChild(doc.createTextNode(""+tablero.getJugador(i).getUltimaTirada()[0]+"+"+tablero.getJugador(i).getUltimaTirada()[1]));
                    jugador.appendChild(ultimaTirada);

                    Element contadorDobles = doc.createElement("contadorDobles");
                    contadorDobles.appendChild(doc.createTextNode(""+tablero.getJugador(i).getContadorDobles()));
                    jugador.appendChild(contadorDobles);

                    Element contCasasEdificadas = doc.createElement("contCasasEdificadas");
                    contCasasEdificadas.appendChild(doc.createTextNode(""+tablero.getJugador(i).getTotalCasas()));
                    jugador.appendChild(contCasasEdificadas);

                    Element contHotelesEdificados = doc.createElement("contHotelesEdificados");
                    contHotelesEdificados.appendChild(doc.createTextNode(""+tablero.getJugador(i).getTotalHoteles()));
                    jugador.appendChild(contHotelesEdificados);
                //end for jugadores
                 }

                Element casillas = doc.createElement("casillas");
		rootElement.appendChild(casillas);
               
                
                //Obtenemos datos Casillas:
                for(int i=0; i< tablero.getCasillas().length; i++) {
                    // jugador elements
                    Element casilla = doc.createElement("casilla");
                    casillas.appendChild(casilla);

                    
                    // set attribute to staff element
                    Attr idCasilla = doc.createAttribute("idCasilla");
                    idCasilla.setValue(Integer.toString(i));
                    casilla.setAttributeNode(idCasilla);
                    
                    
                    //ATASCADO CON EL NULLPOINTER EXCEPTION!!!!!!!!!!!
                    // set attribute to staff element 
                  /*  Attr nombreCasilla = doc.createAttribute("nombreCasilla");
                    nombreCasilla.setTextContent(tablero.getNombreCasilla(i));  
                    casilla.setAttributeNode(nombreCasilla);

                    // shorten way
                    // staff.setAttribute("id", "1");

                    if (tablero.getCasillas()[i].getNombre().substring(0,4).equals("Calle")  ||
                            tablero.getCasillas()[i].getNombre().substring(0,7).equals("Estación") ||
                            tablero.getCasillas()[i].getNombre().substring(0,7).equals("Compañía") ||
                            tablero.getCasillas()[i].getNombre().substring(0,7).equals("Compañía") ||
                            tablero.getCasillas()[i].getNombre().substring(0,4).equals("Plaza") ||
                            tablero.getCasillas()[i].getNombre().substring(0,3).equals("Gran") ||
                            tablero.getCasillas()[i].getNombre().substring(0,7).equals("Glorieta") ||
                            tablero.getCasillas()[i].getNombre().substring(0,4).equals("Ronda") ||
                            tablero.getCasillas()[i].getNombre().substring(0,4).equals("Paseo") ||
                            tablero.getCasillas()[i].getNombre().substring(0,7).equals("Avenida") ) { 
                    Element propietario = doc.createElement("dueño");
                    propietario.appendChild(doc.createTextNode(tablero.getCasillas()[i].getDueño());
                    propietario.appendChild(doc.createTextNode(tablero.getJugador(i).getName()));
                    casilla.appendChild(propietario);
                    } */

                //end for casillas
                 }
                
                
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("C:\\partida.xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);
                System.out.println("\t\033[32m################# FIN DEL JUEGO #####################"); 
		System.out.println("\t\033[32mDatos de la partida grabados correctamente en C:\\partida.xml");

	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
	}
}