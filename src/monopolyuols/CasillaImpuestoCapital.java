/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class CasillaImpuestoCapital extends Casilla {

    /**
     *
     * @param nombre
     */
    public CasillaImpuestoCapital(String nombre) {
		super(nombre);
	}

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            return true;
        }

    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de Impuesto sobre el Capital y se le resta un 10% de su patrimonio total");
                System.out.println(jugador.getName()+" pierde "+jugador.getDinero()*10/100+" euros.");
                jugador.restarDinero(jugador.getDinero()*10/100);
                System.out.println(jugador.getName()+" se queda con "+jugador.getDinero()+" euros.");
                
	}
}
