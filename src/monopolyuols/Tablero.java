/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class Tablero {


        int casillaActual=0;
	int turnoActual = 0;
        //Total de jugadores
	int numeroJugadores = 0;
        String[] nombresJugadores;
	Jugador[] jugadores;    
        Casilla[] casillas = new Casilla[40];
        
        //Dado
        int resultadoDado;

    /**
     *
     * @param numeroJugadores
     * @param nombresJugadores
     */
    public Tablero(int numeroJugadores, String[] nombresJugadores) {
		jugadores= new Jugador[numeroJugadores];
		this.numeroJugadores = numeroJugadores;
                this.nombresJugadores = nombresJugadores;
		for(int i = 0;i < jugadores.length;i++){
                    jugadores[i] = new Jugador(i, nombresJugadores[i]);
		}
         //Definimos Casillas Especiales
        /*o	Casillas Especiales (entre paréntesis el número de casilla).
•	(1) Salida (Cobra 200€, si aún existen propiedades libres)
•	(3) Caja de Comunidad
•	(5) Impuesto Sobre el Capital (El jugador pierde el 10% de su capital y se deposita en el fondo de la banca)
•	(8) Suerte
•	(11) Casilla Cárcel
•	(18) Caja de Comunidad
•	(21) Estacionamiento Gratuito
•	(23) Suerte
•	(31) Ve a la Cárcel
•	(34) Caja de Comunidad
•	(37) Suerte
•	(39) Impuesto de lujo */
                
                casillas[0]= new CasillaSalida("Salida");
                casillas[2]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[4]= new CasillaImpuestoCapital("Impuesto sobre el capital");
                casillas[7]= new CasillaSuerte("Suerte");
                casillas[10]= new CasillaCarcel("Carcel");
                casillas[17]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[20]= new CasillaEstacionamientoGratuito("Estacionamiento gratuito");
                casillas[22]= new CasillaSuerte("Suerte");
                casillas[30]= new CasillaIrACarcel("Ve a la Cárcel");
                casillas[33]= new CasillaCajaComunidad("Caja de Comunidad");
                casillas[36]= new CasillaSuerte("Suerte");
                casillas[38]= new CasillaImpuestoLujo("Impuesto de Lujo");
                
         //Definimos casillas de estaciones y compañias electricas
                casillas[5]= new CasillaEstacion("Estación 1");
                casillas[15]=new CasillaEstacion("Estación 2");
                casillas[25]= new CasillaEstacion("Estación 3"); 
                casillas[35]=new CasillaEstacion("Estación 4");
                casillas[12]= new CasillaCompania("Compañía de electricidad");
                casillas[28]= new CasillaCompania("Compañía de Aguas");
         // A continuación definimosel resto de casillas (calles que serían propiedades con sus respectivos precios)       
                casillas[1]= new CasillaPropiedad("Ronda de Valencia",60,50,100,2,10,30,90,160,250);
                casillas[3]= new CasillaPropiedad("Plaza de Lavapies",60,50,100,4,20,60,180,320,450);
                casillas[6]= new CasillaPropiedad ("Glorieta de Cuatro Caminos",100,50,100,6,30,90,270,400,550);
                casillas[8]= new CasillaPropiedad("Avenida de la Reina Victoria",100,50,100,6,30,90,270,400,550);
                casillas[9]= new CasillaPropiedad("Calle Bravo Murillo",120,50,100,8,40,100,300,450,600);
                casillas[11]= new CasillaPropiedad("Glorieta de Bilbao",140,100,200,10,50,150,450,625,750);
                casillas[13]= new CasillaPropiedad("Calle Alberto Aguilera ",140,100,200,10,50,150,450,625,750);
                casillas[14]= new CasillaPropiedad("Calle de Fuencarral",160,100,200,12,60,180,500,700,900);
                casillas[16]= new CasillaPropiedad("Avenida de Felipe II",180,10,200,14,70,200,550,700,950);
                casillas[18]= new CasillaPropiedad("Calle Velazquez",180,10,200,14,70,200,550,700,950);
                casillas[19]= new CasillaPropiedad("Calle Serrano",200,100,200,16,80,220,600,800,1000);
                casillas[21]= new CasillaPropiedad("Avenida de America",220,150,300,18,90,250,700,875,1050);
                casillas[23]= new CasillaPropiedad("Calle Maria de Molina",220,150,300,18,90,250,700,875,1050);
                casillas[24]= new CasillaPropiedad("Calle de Cea Bermudez",240,150,300,20,100,300,750,925,1100);
                casillas[26]= new CasillaPropiedad("Avenida de los Reyes Catolicos",260,150,300,22,110,330,800,975,1150);
                casillas[27]= new CasillaPropiedad("Calle de Bailen",260,150,300,22,110,330,800,975,1150);
                casillas[29]= new CasillaPropiedad("Plaza de España",280,150,300,24,120,360,850,1025,1200);
                casillas[31]= new CasillaPropiedad("Puerta del Sol",300,200,400,26,130,390,900,1100,1275);
                casillas[32]= new CasillaPropiedad("Calle de Alcala",300,200,400,26,130,390,900,1100,1275);
                casillas[34]= new CasillaPropiedad("Gran Via",320,200,400,28,150,450,1000,1200,1400);
                casillas[37]= new CasillaPropiedad("Paseo de la Castellana",350,200,400,35,175,500,1100,1300,1500);
                casillas[38]= new CasillaPropiedad("Paseo del Prado",400,200,400,50,200,600,1400,1700,2000);
                
        
        }
    public int getCasillaActual() {
        return casillaActual;
    }

    public int getTurnoActual() {
        return turnoActual;
    }

    public String[] getNombresJugadores() {
        return nombresJugadores;
    }

    //la var turnoActual es la id del Jugador (número de jugador)
    public Casilla[] getCasillas() {
        return casillas;
    }   //la var turnoActual es la id del Jugador (número de jugador)
    
    public String getNombreCasilla(int i) {
        return casillas[i].getNombre();
    }
    /**
     *
     * @return
     */
    public Jugador getJugadorActual() {
		return jugadores[turnoActual];
	}
	
    /**
     *
     * @return
     */
    public Jugador[] getJugadores() {
		return jugadores;
	}

    /**
     *
     * @return
     */
    public int getNumeroJugadores () {
            return this.numeroJugadores;
        }

    /**
     *
     */
    public void siguienteTurno() {
		if(++turnoActual >= jugadores.length){
			turnoActual = 0;
		}
	}

    /**
     *
     * @param id
     * @return
     */
    public Jugador getJugador(int id) {
		return jugadores[id];
	}

    /**
     *
     * @return
     */
    public int getResultadoDado() {
		Random rand = new Random();
		int resultadoDado = 1+rand.nextInt(6);
		return resultadoDado;
	}

    /**
     *
     * @param jugador
     * @return
     */
    public int[] tirarDados (Jugador jugador) {
        int dado1 = this.getResultadoDado();
        int dado2 = this.getResultadoDado();
        int total = dado1+dado2;
        int [] resultados  = new int [2];
        resultados[0]=dado1;
        resultados[1]=dado2;
        System.out.println("\t\033[32mHas sacado un "+dado1+" y un "+dado2);
        //System.out.println("\t\033[32mAvanza "+total+" casillas"); 
        jugador.setUltimaTirada(resultados);
        if (dado1 == dado2) {
            jugador.addContadorDobles();
        }
        //return resultados;
        //retornamos resultados como array o total, para usarlo en avanzaCasillaç??
        return resultados;
        
        }
        
    /**
     *
     * @return
     */
    public boolean existenPropiedadesSinDueño () {
            boolean sw = false;
            for (int i=0; i<this.casillas.length; i++) {

                    if (!casillas[i].tieneDueño()) {
                        sw = true;
                        break;
                    }
            }
            return sw;
        }
        
    /**
     *
     * @param jugador
     */
    public void moverJugador(Jugador jugador) {
            int tiradaTotal = jugador.getUltimaTirada()[0]+jugador.getUltimaTirada()[1];
            if (jugador.getCurrentPosition()+ tiradaTotal > 38) {
                jugador.setPosition((jugador.getCurrentPosition() + tiradaTotal)-38);
                if (jugador.getCurrentPosition() > 0 && this.existenPropiedadesSinDueño()){
                            System.out.println("El Jugador "+jugador.getName()+" ha pasado por la casilla de Salida y aun existen propiedades sin dueño, por lo que recibe 200 €");  

                        jugador.añadirDinero(200);
                }
                this.casillas[jugador.getCurrentPosition()].accion(jugador, this);
                this.siguienteTurno();
            }
            else {
                jugador.setPosition(jugador.getCurrentPosition() + tiradaTotal);
                this.casillas[jugador.getCurrentPosition()].accion(jugador, this);
                this.siguienteTurno();
            }
        }
        
    /**
     *
     * @param jugador
     */
    public void procesarCarcel (Jugador jugador) {
            jugador.addTurnoEncarcelado();
            if (jugador.getTurnosEncarcelado() < 3) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Llevas "+jugador.getTurnosEncarcelado()+ " turnos encarcelado");
                System.out.println("¿Deseas pagar 50€ de multa para liberarte o tirar dados? Si sacas dobles quedas libre.");
                char opcionCompra = scanner.nextLine().charAt(0);
                if (opcionCompra == 's' || opcionCompra == 'S') {
                    System.out.println("Has pagado 50 € y has sido liberado de la carcel");
                    jugador.restarDinero(50);
                    jugador.desencarcelar(); 
                }
                else {
                    this.tirarDados(jugador);
                    if (jugador.getUltimaTirada()[0] == jugador.getUltimaTirada()[1] )
                    {
                        System.out.println("Has sacado dobles, quedas liberado de la carcel"); 
                    }
                        }
                }
            else {
                System.out.println("Es tu tercer turno en la carcel, por lo que has de pagar 50 € de multa y quedas liberado"); 
                jugador.restarDinero(50);
                jugador.desencarcelar(); 
            }
            this.siguienteTurno();
        }
}


        
        
