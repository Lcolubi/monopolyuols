/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class MonopolyUOLS {

    /**
     * @param args the command line arguments
     */
Tablero tableroPartida;
public static boolean finDelJuego = false;
int numeroJugadoresArruinados=0;

    /**
     *
     * @param opcionPartida
     */
    public MonopolyUOLS(int opcionPartida){}
           
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("\t\033[32m ¡Bienvenido al Monopoly UOLS creado por Unai, Juan y Luis!\n");
        System.out.println();
        System.out.println("\033[34m########## MENÚ DE INICIO ##########");
        System.out.println("\033[34m#######  1. Nueva Partida  ######### ");
        System.out.println("\033[34m#######  2. Cargar Partida  ######## ");
        System.out.println("\033[34m####################################");

        int opcionPartida = 0;
        Scanner scanner = new Scanner(System.in);
        
        while (opcionPartida < 1 || opcionPartida > 2) {
			try {
                                System.out.println("Introduce el número con la opción deseada:");
                                while (!scanner.hasNextInt()) { //as long as the next is not a int - say you need to input an int and move forward to the next token.
                                    System.out.println("La opción introducida debe de ser un número");
                                    scanner.next();
                                }
				opcionPartida = scanner.nextInt();
			}
			catch(Exception e) {
				System.err.println("\033[31mError: El número introducido es erroneo");
				continue;
			}
			if(opcionPartida > 2 || opcionPartida < 1) {
				System.err.println("\033[31mError: El número introducido debe de ser 1(Nueva partida) o 2(Cargar partida)");
			}
		}
        
        
        if (opcionPartida == 1) {
            System.out.println("\t\033[32mIniciando Nueva Partida...");
            MonopolyUOLS nuevoJuego= new MonopolyUOLS(opcionPartida);
            nuevoJuego.nuevaPartida();
        }
        else if (opcionPartida == 2){
            System.out.println("\t\033[32mCargando partida...");
        }
        else {
            System.err.println("\tError, número seleccionado incorrecto");
        }    
        
    }
    
    /**
     *
     */
    public void nuevaPartida() {
            //Obtenemos el número de jugadores
            int numeroJugadores = 0;
            Scanner scanner2 = new Scanner(System.in);
            
		while (numeroJugadores < 2 || numeroJugadores > 8) {
			try {
				System.out.println("Por favor, selecciona el número de Jugadores");
                                System.out.println("(mínimo 2, máximo: 8");
                                while (!scanner2.hasNextInt()) { //as long as the next is not a int - say you need to input an int and move forward to the next token.
                                    System.out.println("La opción introducida debe de ser un número");
                                    scanner2.next();
                                }
				numeroJugadores = scanner2.nextInt();
			}
			catch(Exception e) {
				System.err.println("Error: Número de jugadores erroneo");
				continue;
			}
			if(numeroJugadores > 8) {
				System.err.println("Error: El número máximo de jugadores es 8");
			}
                        else if (numeroJugadores <2) {
                                System.err.println("Error: El número mínimo de jugadores es 2");
                        }
		}
                
                //Obtenemos el nombre de los jugadores
                String[] nombresJugadores = new String[numeroJugadores];
                for(int i = 0;i < nombresJugadores.length;i++){
                    int numJugador=i+1;
                    Scanner scannerNombre = new Scanner(System.in);
                    System.out.println("Introduce el nombre del Jugador "+numJugador);
                    nombresJugadores[i]= scannerNombre.nextLine();
                }
                
                System.out.println("\t\033[32mIniciando el juego para "+numeroJugadores+" jugadores:");
                for(int i = 0;i < nombresJugadores.length;i++){
                int numJugador=i+1;
                System.out.println("\033[34mJugador "+numJugador+": "+nombresJugadores[i]);
                }
                
               tableroPartida = new Tablero(numeroJugadores, nombresJugadores);
               //int [] resultadoDados = tableroPartida.tirarDados();
               this.iniciarPartida(tableroPartida);
	}
        
    /**
     *
     * @param tablero
     */
    public void iniciarPartida(Tablero tablero) {
            System.out.println("\t\033[32m################# COMIENZA LA PARTIDA #####################"); 
            while (!this.finDelJuego) {
                
                if (!tablero.getJugadorActual().estaArruinado()) { //Si tiene dinero jugar:
                    System.out.println("\t\033[32mTurno de el jugador "+tablero.getJugadorActual().getName()); 
                    System.out.println("\t\033[32mDinero total del jugador "+tablero.getJugadorActual().getName()+" : "+tablero.getJugadorActual().getDinero()+" €"); 
                    //COMPROBACION CARCEL
                    if (tablero.getJugadorActual().getEncarcelado()) {
                        tablero.procesarCarcel(tablero.getJugadorActual());
                    }
                    else {
                    tablero.tirarDados(tablero.getJugadorActual());
                    tablero.moverJugador(tablero.getJugadorActual());
                    }
                }
                else {
                    tablero.siguienteTurno();
                }
                
                //FIN DEL JUEGO SI QUIEBRAN TODOS LOS JUGADORES MENOS 1
                Jugador jugadorMasRico = null;
                int dineroMax=0;
                for (int i=0; i< tablero.getJugadores().length; i++) {
                    if (tablero.getJugador(i).getDinero()<0)
                    {
                        this.numeroJugadoresArruinados++;
                    }
                    if (tablero.getJugador(i).getDinero() > dineroMax) {
                        dineroMax = tablero.getJugador(i).getDinero();
                        jugadorMasRico = tablero.getJugador(i);  
                    }  
                }
                
                if (this.numeroJugadoresArruinados >= tablero.getNumeroJugadores()-1)
                {
                    System.out.println("\t\033[32m################# FIN DEL JUEGO #####################"); 
                    System.out.println("\t\033[32mEl jugador ganador es "+ jugadorMasRico.getName() + " con un total de "+jugadorMasRico.getDinero()+" €"); 
                    this.finDelJuego = true;
                    
                }
                
            }
        }
    
}
