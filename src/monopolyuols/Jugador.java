/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public class Jugador {
    
        int totalTurnosJugados = 0;
	int posicion = 0;
	int id;
	String nombre;
        //Dinero inicial --> 1500 euros
	int dinero = 1500;
        // Saber si está encarcelado
        boolean encarcelado = false;
        int cartaLibreCarcel = 0; //Carta Suerte Liberar de Carcel
        int turnosEncarcelado; //numero de turnos que lleva encarcelado
        //Numero de Estaciones y compañias que posee
        int numeroEstaciones = 0;
        int numeroComp = 0;
        int [] ultimaTirada = new int[2]; //Los dos dados tirados en la ultima jugada
        int contadorDobles; //Numero de veces que se han sacado dobles dados en un turno, si se sacan 3 veces seguidas se va a la carcel
        int contCasasEdificadas = 0;
        int contHotelesEdificados = 0;
        
    /**
     *
     * @param id
     * @param name
     */
    public Jugador(int id, String name) {
		this.id = id;
		this.nombre = name;
	}

    /**
     *
     * @return
     */
    public boolean getEncarcelado (){
            return encarcelado;
        }

    /**
     *
     */
    public void encarcelar(){
            this.encarcelado=true;
            this.posicion = 10;
        }

    /**
     *
     */
    public void desencarcelar() {
            this.encarcelado=false;
            this.turnosEncarcelado = 0;
        }

    /**
     *
     * @return
     */
    public int getCartaLibreCarcel () {
            return this.cartaLibreCarcel;
        }

    /**
     *
     */
    public void gastarCartaLibreCarcel () {
            this.cartaLibreCarcel --;
        }

    /**
     *
     */
    public void añadirCartaLibreCarcel(){
            this.cartaLibreCarcel ++;
        }
        
    /**
     *
     * @return
     */
    public int getTurnosJugados() {
		return totalTurnosJugados;
	}
	
    /**
     *
     * @return
     */
    public int getCurrentPosition() {
		return posicion;
	}
	
    /**
     *
     * @param posicion
     */
    public void setPosition(int posicion) {
		this.posicion = posicion;
	}
	
    /**
     *
     */
    public void nextTurn() {
		totalTurnosJugados++;
	}
	
    /**
     *
     * @return
     */
    public String getName() {
		return nombre;
	}
	
    /**
     *
     * @return
     */
    public int getDinero() {
		return this.dinero;
	}
        
    /**
     *
     * @param cantidad
     */
    public void añadirDinero(int cantidad) {
		this.dinero += cantidad;
	}
	
    /**
     *
     * @param cantidad
     */
    public void restarDinero(int cantidad) {
		this.dinero -= cantidad;
	}

    /**
     *
     * @return
     */
    public int getID() {
		return id;
	}

    /**
     *
     * @return
     */
    public int getNumEstaciones() {
		return this.numeroEstaciones;
	}

    /**
     *
     * @return
     */
    public int getNumComp() {
		return this.numeroComp;
	}

    /**
     *
     */
    public void addEstacion () {
            this.numeroEstaciones++;
        }

    /**
     *
     */
    public void addComp() {
            this.numeroComp++;
        }

    /**
     *
     * @return
     */
    public int [] getUltimaTirada() {
            return this.ultimaTirada;
        }    

    /**
     *
     * @param tirada
     */
    public void setUltimaTirada (int [] tirada) {
            this.ultimaTirada = tirada;
        }

    /**
     *
     * @return
     */
    public int getContadorDobles() {
            return this.contadorDobles;
        }

    /**
     *
     */
    public void setContadorDoblesNull() {
            this.contadorDobles = 0;
        }

    /**
     *
     */
    public void addContadorDobles () {
            this.contadorDobles ++;
        }

    /**
     *
     * @return
     */
    public int getTurnosEncarcelado () {
            return this.turnosEncarcelado;
        }

    /**
     *
     */
    public void addTurnoEncarcelado () {
            this.turnosEncarcelado++;
        }

    /**
     *
     */
    public void setTurnosEncarceladoNull() {
            this.turnosEncarcelado = 0;
        }

    /**
     *
     * @return
     */
    public int getTotalCasas() {return this.contCasasEdificadas;}

    /**
     *
     * @return
     */
    public int getTotalHoteles() {return this.contHotelesEdificados;}

    /**
     *
     */
    public void añadirContCasa(){this.contCasasEdificadas++;}

    /**
     *
     */
    public void añadirContHotel(){this.contHotelesEdificados++;}

    /**
     *
     */
    public void restarContCasa(){this.contCasasEdificadas--;}
        
    /**
     *
     * @return
     */
    public boolean estaArruinado () {
            if (this.dinero < 0) {
                return true;
            }
            else {
                return false;
            }
        }
        
        
}
