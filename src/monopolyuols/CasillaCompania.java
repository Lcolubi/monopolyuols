/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class CasillaCompania extends Casilla {
        //ID DEL DUEÑO
        Jugador dueño = null;  
        
    /**
     *
     * @param nombre
     */
    public CasillaCompania(String nombre) {
		super(nombre);
	}

    /**
     *
     * @param dueño
     */
    public void setDueño (Jugador dueño) {
            this.dueño = dueño;
        }

    /**
     *
     * @return
     */
    public Jugador getDueño() {
            return this.dueño;
        }

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            boolean sw=false;
            if (this.dueño !=null) {
                sw = true;
            }
            return sw;
        }

    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de "+ super.getNombre());
                if (this.dueño == null) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("¿Deseas comprar la Compañía por 150 Euros? (S/N)");
                    String opcionCompra;
                    while (true) {
     opcionCompra = scanner.nextLine().trim().toLowerCase();
                        if (opcionCompra.equals("g"))
                        {
                        WriteXMLFile.writeXML(tablero);
                        MonopolyUOLS.finDelJuego = true;
                        break;
                        }
                        else if (opcionCompra.equals("s")) {
                          break;
                        } else if (opcionCompra.equals("n")) {
                          break;
                        } else {
                           System.out.println("La opción introducida debe de ser 'S' (sí) o 'N' (no)");
                        }
                    }
                    if (opcionCompra.equals("s")) {
                        jugador.restarDinero(150);
                        int idJugador = jugador.getID();
                        this.setDueño(jugador);
                        jugador.addComp();
                        System.out.println("El jugador "+jugador.getName()+" ha comprado la Casilla de "+super.getNombre()
                                +" por 150 Euros.");
                    }
                    else if (opcionCompra.equals("g")){
                        System.out.println("Datos de la partida guardados");
                    }
                    else {
                        System.out.println("El jugador "+jugador.getName()+" decide no comprar la Compañía");
                    }
                }
                else {
                    System.out.println("Esta casilla pertenece al Jugador "+this.dueño.getName()+"que tiene el siguiente número de compañías: "+this.dueño.getNumComp());
                    
                    //SUMAMOS LOS 2 DADOS DE LA ULTIMA TIRADA DEL JUGADOR
                    int tiradaTotalDados = jugador.getUltimaTirada()[0] + jugador.getUltimaTirada()[1];
                    
                    if (this.dueño.getNumComp() == 1){
                        System.out.println("Debes de pagarle un total de "+tiradaTotalDados*4+" Euros.");
                        jugador.restarDinero(tiradaTotalDados*4);
                        this.dueño.añadirDinero(tiradaTotalDados*4);
                    } else if (this.dueño.getNumComp() == 2) {
                        System.out.println("Debes de pagarle un total de "+tiradaTotalDados*10+" Euros.");
                        jugador.restarDinero(tiradaTotalDados*10);
                        this.dueño.añadirDinero(tiradaTotalDados*10);
                    }

                }
	}
}
