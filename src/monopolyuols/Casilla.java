/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

/**
 *
 * @author lcolubi
 */
public abstract class Casilla {
    	String nombre;
	
    /**
     *
     * @param nombre
     */
    public Casilla(String nombre) {
		this.nombre = nombre;
	}
	
    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param jugador
     * @param tablero
     */
    public abstract void accion(Jugador jugador, Tablero tablero);

    /**
     *
     * @return
     */
    public abstract boolean tieneDueño();
}
