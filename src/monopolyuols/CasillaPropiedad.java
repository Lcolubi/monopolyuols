/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Scanner;

/**
 *
 * @author lcolubi
 */
public class CasillaPropiedad extends Casilla {
        //ID DEL DUEÑO
        Jugador dueño = null;  
        int costeSolar; //precio
        int costeEdificarCasa;
        int costeEdificarHotel;
        int precioAlquilerSolar;
        int precioAlquiler1Casa;
        int precioAlquiler2Casas;
        int precioAlquiler3Casas;
        int precioAlquiler4Casas;
        int precioAlquilerHotel;
        int precioActualAlquiler;
        int precioActualConstruir;
        /* El estado de casilla es el estado de edificación de esta
        0 -> Sin comprar
        1 -> Solar comprado
        2 -> 1 casa
        3 -> 2 casas
        4 -> 3 casas
        5 -> 4 casas
        6 -> Edificado Hotel
        */ 
        int estadoCasilla = 0;
        String estadoCasillaString;
    
    /**
     *
     * @param nombre
     * @param costeSolar
     * @param costeEdificarCasa
     * @param costeEdificarHotel
     * @param precioAlquilerSolar
     * @param precioAlquiler1Casa
     * @param precioAlquiler2Casas
     * @param precioAlquiler3Casas
     * @param precioAlquiler4Casas
     * @param precioAlquilerHotel
     */
    public CasillaPropiedad(String nombre, int costeSolar, int costeEdificarCasa, int costeEdificarHotel,
                int precioAlquilerSolar, int precioAlquiler1Casa, int precioAlquiler2Casas, int precioAlquiler3Casas,
                int precioAlquiler4Casas, int precioAlquilerHotel) {
		super(nombre);
                this.costeSolar = costeSolar;
                this.costeEdificarCasa = costeEdificarCasa;
                this.costeEdificarHotel = costeEdificarHotel;
                this.precioAlquilerSolar = precioAlquilerSolar;
                this.precioAlquiler1Casa = precioAlquiler1Casa;
                this.precioAlquiler2Casas = precioAlquiler2Casas;
                this.precioAlquiler3Casas = precioAlquiler3Casas;
                this.precioAlquiler4Casas = precioAlquiler4Casas;
                this.precioAlquilerHotel = precioAlquilerHotel;
                
	}

    /**
     *
     * @param dueño
     */
    public void setDueño (Jugador dueño) {
            this.dueño = dueño;
        }

    /**
     *
     * @return
     */
    public Jugador getDueño() {
            return this.dueño;
        }

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            boolean sw=false;
            if (this.dueño !=null) {
                sw= true;
            }
            return sw;
            
        }

    /**
     *
     */
    public void updateEstadoCasilla() {
                switch (this.estadoCasilla) {
                        case 1:
                            this.estadoCasillaString = "un solar sin edificar";
                            this.precioActualAlquiler = this.precioAlquilerSolar;
                            this.precioActualConstruir = this.costeEdificarCasa;
                            break;
                        case 2:
                            this.estadoCasillaString = "1 casa edificada";
                            this.precioActualAlquiler = this.precioAlquiler1Casa;
                            this.precioActualConstruir = this.costeEdificarCasa;
                            break;
                        case 3:
                            this.estadoCasillaString = "2 casas edificadas";
                            this.precioActualAlquiler = this.precioAlquiler2Casas;
                            this.precioActualConstruir = this.costeEdificarCasa;
                            break;
                        case 4:
                            this.estadoCasillaString = "3 casas edificadas";
                            this.precioActualAlquiler = this.precioAlquiler3Casas;
                            this.precioActualConstruir = this.costeEdificarCasa;
                            break;
                        case 5:
                            this.estadoCasillaString = "4 casas edificadas";
                            this.precioActualAlquiler = this.precioAlquiler4Casas;
                            this.precioActualConstruir = this.costeEdificarHotel;
                            break;
                        case 6:
                            this.estadoCasillaString = "1 hotel edificado";
                            this.precioActualAlquiler = this.precioAlquilerHotel;
                            break;
                    }
        }
	
    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de "+ super.getNombre());
                this.updateEstadoCasilla();
                //CASO 1 : Casilla sin dueño
                if (this.dueño == null) {     
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("¿Deseas comprar la Propiedad por "+this.costeSolar+"€ ? (S/N)");
                    String opcionCompra;

                    while (true) {
                        opcionCompra = scanner.nextLine().trim().toLowerCase();
                        if (opcionCompra.equals("g"))
                        {
                        WriteXMLFile.writeXML(tablero);
                        MonopolyUOLS.finDelJuego = true;
                        break;
                        }
                        else if (opcionCompra.equals("s")) {
                          break;
                        } else if (opcionCompra.equals("n")) {
                          break;
                        } else {
                           System.out.println("La opción introducida debe de ser 'S' (sí) o 'N' (no)");
                        }
                    }
                    if (opcionCompra.equals("s")) {
                        jugador.restarDinero(this.costeSolar);
                        this.setDueño(jugador);
                        jugador.addComp();
                        this.estadoCasilla++;
                        System.out.println("El jugador "+jugador.getName()+" ha comprado la Casilla de "+super.getNombre()+" por "+this.costeSolar+"€");
                    }
                    else if (opcionCompra.equals("g")){
                        System.out.println("Datos de la partida guardados");
                    }
                    else {
                        System.out.println("El jugador "+jugador.getName()+" decide no comprar la Propiedad");
                    }
                }
                // CASO 2: EL DUEÑO CAE EN UNA CASILLA DE SU PROPIEDAD
                else if (jugador.getID() == this.dueño.getID()){
                    this.updateEstadoCasilla();
                    System.out.println("La casilla de "+super.getNombre()+"forma parte de tus propiedades");
                    System.out.println("Actualmente cuentas con "+this.estadoCasillaString);
                    
                    //Comprobacion para no dejar edificar más si ya hay hotel edificado
                    if (this.estadoCasilla<6) { 
                        Scanner scanner = new Scanner(System.in);
                        System.out.println("¿Deseas edificar en la propiedad por"+this.precioActualConstruir+"€ ? (S/N)");
                        char opcionConstruir = scanner.nextLine().charAt(0);
                        if (opcionConstruir == 's' || opcionConstruir== 'S') {
                            jugador.restarDinero(this.precioActualConstruir);
                            this.estadoCasilla++;
                            this.updateEstadoCasilla();
                            //Actualizamos contadores
                            if (this.estadoCasilla==5) {
                                jugador.añadirContHotel();
                                jugador.restarContCasa();
                            } else {jugador.añadirContCasa(); }
                            
                            System.out.println("El jugador "+jugador.getName()+" ha decidido edificar en "+super.getNombre()+" por"+this.precioActualConstruir+"€");
                            System.out.println("Tras elegir la opción de edificar, en la casilla "+super.getNombre()+" hay "+this.estadoCasillaString);
                        }
                        else {
                            System.out.println("El jugador "+jugador.getName()+" decide no edificar");
                        }
                    }
                }
                //CASO 3: EL JUGADOR CAE EN UNA CASILLA CON DUEÑO AJENO
                else {
                    System.out.println("Esta casilla pertenece al Jugador "+this.dueño.getName());
                        System.out.println("Debes de pagarle un total de "+this.precioActualAlquiler+" Euros. de alquiler");
                        jugador.restarDinero(this.precioActualAlquiler);
                        this.dueño.añadirDinero(this.precioActualAlquiler);
                }
                
                this.updateEstadoCasilla();
	}
}
