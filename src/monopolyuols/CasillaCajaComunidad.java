/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;

/**
 *
 * @author lcolubi
 */
public class CasillaCajaComunidad extends Casilla{

    /**
     *
     * @param nombre
     */
    public CasillaCajaComunidad(String nombre) {
            super(nombre);
		
	}

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            return true;
        }

    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                Random rand = new Random();
		int resultadoSuerte = 1+rand.nextInt(16);
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de Suerte y recibe la siguiente tarjeta:");    
                
                switch (resultadoSuerte) {
                    case 1:
                        System.out.println("Mueve hasta la casilla de salida");
                        jugador.setPosition(0);
                        tablero.casillas[0].accion(jugador, tablero);
                        
                        break;
                    case 2:
                        System.out.println("Error del banco ganas 200€.");
                        jugador.añadirDinero(200);
                        break;
                        
                    case 3:
                        System.out.println("Pagas al dentista 50€.");
                        jugador.restarDinero(50);
                        break;
                                
                    case 4:
                        System.out.println("Ganas 50€ de dividendos");
                        jugador.añadirDinero(50);
                        break;  
                        
                    case 5:
                        System.out.println("Queda libre de la cárcel, esta carta se puede usar cuando se crea oportuno. No se puede vender.");
                        jugador.añadirCartaLibreCarcel();
                        break; 
                        
                    case 6:
                        System.out.println("Ve a la cárcel.");
                        jugador.encarcelar();
                        break;  
                        
                    case 7:
                        System.out.println("Gran estreno en la ópera, cada jugador te paga 50€.");
                        for (int i=0; i<tablero.getJugadores().length;i++) {
                            if (tablero.getJugadores()[i].getID() != jugador.getID()){
                                tablero.getJugadores()[i].restarDinero(50);
                                jugador.añadirDinero(50);
                            }
                        }
                        break;           
                    case 8:
                        System.out.println("Dividendos. Ganas 100 Euros");
                        jugador.añadirDinero(100);
                        //mover a la carcel:
                        break; 
                        
                    case 9:
                        System.out.println("Devolución de impuestos. Ganas 20 Euros");
                        jugador.añadirDinero(20);
                        break;  
                        
                    case 10:
                        System.out.println("¡Feliz cumpleaños! Ganas 10 Euros de cada jugador");
                        for (int i=0; i<tablero.getJugadores().length;i++) {
                            if (tablero.getJugadores()[i].getID() != jugador.getID()){
                                tablero.getJugadores()[i].restarDinero(10);
                                jugador.añadirDinero(10);
                            }
                        }
                        break;    
                        
                    case 11:
                        System.out.println("Primer premio en la loteria. Ganas 100 Euros");
                        jugador.añadirDinero(100);
                        break;  
                        
                    case 12:
                        System.out.println("Multa de velocidad. Pagas 100 Euros");
                        jugador.restarDinero(100);
                        break;
                    
                    case 13:
                        System.out.println("Matricula escolar, pagas 150 Euros");
                        jugador.restarDinero(150); 
                        break;
                        
                    case 14:
                        System.out.println("Tasa de consultor, ganas 25 Euros");
                        jugador.añadirDinero(25);   
                        break;
                        
                    case 15:
                        System.out.println("Reparaciones. Pagas 40 euros por cada casa que tengas y 150 Euros por cada Hotel");
                        //METODO A IMPLEMENTAR
                        System.out.println("Tienes un total de "+jugador.getTotalCasas()+" casas y "+jugador.contHotelesEdificados+" hoteles edificados");
                        System.out.println("En total debes de pagar "+(jugador.getTotalCasas()*40 + jugador.getTotalHoteles()*150)+" € de reparaciones");
                        jugador.restarDinero((jugador.getTotalCasas()*40 + jugador.getTotalHoteles()*150));
                        break;
                        
                    case 16:
                        System.out.println("Ganas el segundo premio en un concurso de disfraces ganas 10€. ");
                        jugador.añadirDinero(10);
                       
                }
	}
}
