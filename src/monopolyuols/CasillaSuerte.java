/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopolyuols;

import java.util.Random;

/**
 *
 * @author lcolubi
 */
public class CasillaSuerte extends Casilla {

    /**
     *
     * @param nombre
     */
    public CasillaSuerte(String nombre) {
		super(nombre);
	}

    /**
     *
     * @return
     */
    public boolean tieneDueño () {
            return true;
        }

    /**
     *
     * @param jugador
     * @param tablero
     */
    public void accion(Jugador jugador, Tablero tablero) {
                Random rand = new Random();
		int resultadoSuerte = 1+rand.nextInt(14);
                System.out.println("El Jugador "+jugador.getName()+" ha caido en la casilla de Suerte y recibe la siguiente tarjeta:");    
                
                switch (resultadoSuerte) {
                    case 1:
                        System.out.println("Mueve hasta la casilla de salida");
                        jugador.setPosition(0);
                        tablero.casillas[0].accion(jugador, tablero);
                        break;
                    case 2:
                        System.out.println("Mueve hasta la última propiedad del tablero.");
                        //Metodo funcional
                        jugador.setPosition(38);
                        tablero.casillas[38].accion(jugador, tablero);
                        break;
                        
                    case 3:
                        System.out.println("Mueve hasta la casilla de servicio público más próxima, si tiene propietario paga como si hubieras sacado un 10 en la tirada. Si está libre puedes adquirirla.");
                        int [] resultados  = new int [2];
                            resultados[0]=5;
                            resultados[1]=5;
                        //Metodo funcional
                        if (jugador.getCurrentPosition() >= 0 && jugador.getCurrentPosition() <= 10) {
                            jugador.setPosition(12);
                            jugador.setUltimaTirada(resultados);
                            tablero.casillas[12].accion(jugador, tablero);
                        } else {
                            jugador.setPosition(28);
                            jugador.setUltimaTirada(resultados);
                            tablero.casillas[28].accion(jugador, tablero);
                        }
                        
                        break;
                                
                    case 4:
                        System.out.println("Mueve hasta la estación más próxima, si tiene propietario págale el alquiler. Si está libre puedes adquirirla..");
                        //Metodo funcional
                        if (jugador.getCurrentPosition() >= 0 && jugador.getCurrentPosition() <= 10) {
                            jugador.setPosition(5);
                            tablero.casillas[5].accion(jugador, tablero);
                        } else if (jugador.getCurrentPosition() > 10 && jugador.getCurrentPosition() <= 20) {
                            jugador.setPosition(15);
                            tablero.casillas[15].accion(jugador, tablero);
                        } else if (jugador.getCurrentPosition() > 20 && jugador.getCurrentPosition() <= 30) {
                            jugador.setPosition(25);
                            tablero.casillas[25].accion(jugador, tablero);
                        } else {
                            jugador.setPosition(35);
                            tablero.casillas[35].accion(jugador, tablero);
                        }
                        
                        break;                            
                    case 5:
                        System.out.println("El banco paga un dividendo de 50€.");
                        jugador.añadirDinero(50);
                        
                        break;                
                    case 6:
                        System.out.println("Queda libre de la cárcel, esta carta se puede usar cuando se crea oportuno. No se puede vender.");
                        jugador.añadirCartaLibreCarcel();
                        
                        break;                        
                    case 7:
                        System.out.println("Retrocede 3 casillas.");
                        jugador.setPosition(jugador.getCurrentPosition()-3);
                        tablero.casillas[jugador.getCurrentPosition()].accion(jugador, tablero);
                        
                        break;           
                    case 8:
                        System.out.println("Ve a la cárcel, sin cobrar si se pasa por la casilla de salida.");

                        jugador.encarcelar();

                        break;             
                    case 9:
                        System.out.println("Haz reparaciones en tus propiedades, 25€ por casa y 100€ por hotel.");
                        System.out.println("Tienes un total de "+jugador.getTotalCasas()+" casas y "+jugador.contHotelesEdificados+" hoteles edificados");
                        System.out.println("En total debes de pagar "+(jugador.getTotalCasas()*20 + jugador.getTotalHoteles()*100)+" € de reparaciones");
                        jugador.restarDinero((jugador.getTotalCasas()*20 + jugador.getTotalHoteles()*100));
                        
                        break;             
                    case 10:
                        System.out.println("+Paga impuestos 15€.");
                        jugador.restarDinero(15);
                        break;             
                    case 11:
                        System.out.println("Ve a la primera estación del tablero.");
                        jugador.setPosition(5);
                        
                        
                        break;             
                    case 12:
                        System.out.println("Cada jugador te paga 50€.");
                        //Metodo funcional
                        for (int i=0; i<tablero.getJugadores().length;i++) {
                            if (tablero.getJugadores()[i].getID() != jugador.getID()){
                                tablero.getJugadores()[i].restarDinero(50);
                                jugador.añadirDinero(50);
                            }
                        }
                        break;
                    
                    case 13:
                        System.out.println("Dividendos, ganas 150€.");
                        jugador.añadirDinero(150); 
                        
                    case 14:
                        System.out.println("Ganas una competición de crucigramas (+100Euros)");
                        jugador.añadirDinero(100);
                }
	}
}
